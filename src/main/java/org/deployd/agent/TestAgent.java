package org.deployd.agent;

import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper;
import net.bytebuddy.description.annotation.AnnotationDescription;
import net.bytebuddy.matcher.ElementMatchers;
import net.lightbody.bmp.BrowserMobProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.lang.instrument.Instrumentation;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static net.bytebuddy.matcher.ElementMatchers.isConstructor;

/**
 * Agent to inject testing framework specific implementations.
 */
public class TestAgent {

    public static final Logger logger = LoggerFactory.getLogger(TestAgent.class);

    public static Map<String, BrowserMobProxy> proxyConcurrentMap = new ConcurrentHashMap<>();

    private TestAgent() {
    }

    // in case we use javassist instead of bytebuddy
//    public static void premain(String arguments, Instrumentation instrumentation) {
//        System.out.println("[ Initializing Java Agent ]");
//        TestNgTransformer transformer = new TestNgTransformer();
//        System.out.println("arguments: " + arguments);
//        transformer.toWatchFor = "org/deployd/test";
//        instrumentation.addTransformer(transformer);
//        System.out.println("[ Finished Initializing ]");
//    }

    public static void premain(String arguments,
                               Instrumentation instrumentation) {
        System.setProperty("net.bytebuddy.experimental", "true");
        logger.info("***** initializing agent ***** {}", arguments);
        final Advice testMethodAdvice = Advice.to(TestNgAdvice.class);

        new AgentBuilder.Default()
                //.disableClassFormatChanges()
                .with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
                .with(AgentBuilder.TypeStrategy.Default.REDEFINE)
                .with(AgentBuilder.TypeStrategy.Default.REBASE)

                .type(ElementMatchers.nameContains("org.deployd"))
                .transform((builder, typeDescription, classLoader, module) ->
                        builder.annotateType(
                                AnnotationDescription.Builder.ofType(Listeners.class)
                                        .defineTypeArray("value", TestNgListener.class)
                                        .build())
                        .visit(new AsmVisitorWrapper.ForDeclaredMethods().method(ElementMatchers.isMethod().and(ElementMatchers.isAnnotatedWith(Test.class)), testMethodAdvice)))

//                .type(ElementMatchers.nameContains("org.deployd"))
//                .transform((builder, typeDescription, classLoader, module) ->
//                        builder.method(isAnnotatedWith(Test.class)).intercept(MethodDelegation.to(ProxyPerMethod.class)))

                .type(ElementMatchers.nameMatches("org.openqa.selenium.firefox.FirefoxDriver"))
                .transform((builder, typeDescription, classLoader, module) ->
                        builder.visit(Advice.to(FirefoxDriverConstructorAdvice.class).on(isConstructor()))
                )

                .type(ElementMatchers.nameMatches("org.openqa.selenium.chrome.ChromeDriver"))
                .transform((builder, typeDescription, classLoader, module) ->
                        builder.visit(Advice.to(ChromeDriverConstructorAdvice.class).on(isConstructor()))
                )

                //.with(AgentBuilder.Listener.StreamWriting.toSystemOut())
                .installOn(instrumentation);
    }
}