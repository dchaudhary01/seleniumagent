package org.deployd.agent;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.implementation.bytecode.assign.Assigner;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.deployd.agent.TestAgent.proxyConcurrentMap;

public class ChromeDriverConstructorAdvice {

    public static final Logger logger = LoggerFactory.getLogger(ChromeDriverConstructorAdvice.class);
    private ChromeDriverConstructorAdvice(){}
    public static ThreadLocal<BrowserMobProxy> constructedProxyPort = new ThreadLocal<>();

    @Advice.OnMethodEnter
    public static void before(
            @Advice.Origin String method,
            @Advice.AllArguments(readOnly = false, typing = Assigner.Typing.DYNAMIC) Object[] args) {
        logger.info("-----> instantiating chromedriver: {} {}", method, args);
        if (null == args || args.length == 0) {
            logger.info(" this will be handled with super call");
        } else {
            for (int k = 0; k < args.length; k++) {
                logger.info(" receiving instance for further processing--> {}", args[k].getClass());
                if (args[k] instanceof ChromeOptions) {
                    ChromeOptions chromeOptionsDummy = (ChromeOptions)args[k];
                    logger.info(" -- preoptiosn: {}", chromeOptionsDummy.getCapability(CapabilityType.PROXY));
                    logger.info(" -- {}", args[k]);
                    if ( null== chromeOptionsDummy.getCapability(CapabilityType.PROXY)) {
                        BrowserMobProxy proxy = new BrowserMobProxyServer();
                        ChromeOptions chromeOptions = ((ChromeOptions) args[0]).merge(AgentUtil.getChromeProxyCapabilities(proxy));
                        constructedProxyPort.set(proxy);
                        args[k] = chromeOptions;
                    }
                }
            }
        }
        logger.info("-----> pre args: {} length: {}", args, args.length);
    }

    @Advice.OnMethodExit
    public static void after(
            @Advice.Origin String method,
            @Advice.This Object thisObject
    ) {
        logger.info("<----- instantiated chromedriver: {} {}", method, thisObject);
        if (thisObject instanceof ChromeDriver) {
            ChromeDriver instantiatedObject = (ChromeDriver) thisObject;
            logger.info("instantiatedObject: {} {}", instantiatedObject.getSessionId(), constructedProxyPort.get());
            proxyConcurrentMap.put(instantiatedObject.getSessionId().toString(), constructedProxyPort.get());
            logger.info(" capabilities on this instance {}", instantiatedObject.getCapabilities().asMap());
        } else {
            logger.error("cannot identify webdriver instance / proxy will instance will not be available");
        }
    }
}
