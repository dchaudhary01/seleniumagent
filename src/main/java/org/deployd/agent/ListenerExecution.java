package org.deployd.agent;

import org.testng.ITestResult;

public interface ListenerExecution {
    public void assignTestIDForCurrentExecution(ITestResult result);
}
