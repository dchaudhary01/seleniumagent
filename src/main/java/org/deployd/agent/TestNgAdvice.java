package org.deployd.agent;

import net.bytebuddy.asm.Advice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Main advice applied to all instrumented classes and methods
 */
public class TestNgAdvice {

    public static final Logger logger = LoggerFactory.getLogger(TestNgAdvice.class);

    private TestNgAdvice() {
    }

    @Advice.OnMethodEnter
    public static void before(
            //@Advice.This Object thisObject,
            @Advice.Origin String method,
            @Advice.Origin("#t #m") String detaildOrigin,
            //@Advice.Origin("#t") String className,
            //@Advice.Origin("#m") String methodName,
            @Advice.AllArguments Object[] args) {

        logger.info("-----> before execution: {}", method);
    }

    @Advice.OnMethodExit
    public static void after(//@Advice.This Object thisObject,
                             @Advice.Origin String method,
                             @Advice.AllArguments Object[] args
    ) {
        logger.info("<----- after execution: {}", method);
    }
}
