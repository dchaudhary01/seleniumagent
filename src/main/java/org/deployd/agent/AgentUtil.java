package org.deployd.agent;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.client.ClientUtil;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgentUtil {

    public static final Logger logger = LoggerFactory.getLogger(AgentUtil.class);

    private AgentUtil(){}

    public static Capabilities getFirefoxProxyCapabilities(BrowserMobProxy browserMobProxy) {
        logger.info("current proxy instance is: {}", browserMobProxy);
        if (!browserMobProxy.isStarted()) {
            logger.info("attempting proxy start: {}", browserMobProxy);
            browserMobProxy.start(0);
        }
        logger.info("proxy is up - ready for further configuration");
        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(browserMobProxy);

        FirefoxProfile profile = new FirefoxProfile();
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setAcceptInsecureCerts(true);
        cap.setCapability("dom.push.enabled", false);
        String host = seleniumProxy.getHttpProxy().split(":")[0];
        int port = Integer.parseInt(seleniumProxy.getHttpProxy().split(":")[1]);
        profile.setPreference("network.proxy.type", 1);
        profile.setPreference("network.proxy.http", host);
        profile.setPreference("network.proxy.http_port", port);
        profile.setPreference("network.proxy.ssl", host);
        profile.setPreference("network.proxy.ssl_port", port);
        profile.setPreference("acceptInsecureCerts", true);
        profile.setPreference("dom.push.enabled", false);
        cap.setCapability(FirefoxDriver.PROFILE, profile);
        return cap;
    }

    public static Capabilities getChromeProxyCapabilities(BrowserMobProxy browserMobProxy) {
        logger.info("current proxy instance is: {}", browserMobProxy);
        if (!browserMobProxy.isStarted()) {
            logger.info("attempting proxy start: {}", browserMobProxy);
            browserMobProxy.start(0);
        }
        logger.info("proxy is up - ready for further configuration");
        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(browserMobProxy);
        seleniumProxy.setHttpProxy("localhost:"+ browserMobProxy.getPort());
        seleniumProxy.setSslProxy("localhost:"+ browserMobProxy.getPort());

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability(CapabilityType.PROXY, seleniumProxy);
        chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        chromeOptions.setCapability (CapabilityType.ACCEPT_INSECURE_CERTS, true);
        return chromeOptions;
    }
}
