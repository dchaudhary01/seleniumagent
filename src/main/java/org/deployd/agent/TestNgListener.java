package org.deployd.agent;


import net.lightbody.bmp.BrowserMobProxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.lang.reflect.Field;
import java.util.Optional;

import static org.deployd.agent.TestAgent.proxyConcurrentMap;


/**
 * TestNg specific listener configuration.
 */
public class TestNgListener implements ITestListener {

    public static final Logger logger = LoggerFactory.getLogger(TestNgListener.class);
    ThreadLocal<BrowserMobProxy> currentListenerAssignedProxy = new ThreadLocal<>();

    @Override
    public void onTestStart(ITestResult result) {
        ListenerExecution listenerExecution = result1 -> {
            String currentExecutionID = result1.getTestClass() + result1.getName();
            WebDriver webDriver;
            try {
                Field[] fieldsInTestClass = result1.getTestClass().getRealClass().getDeclaredFields();
                for (Field testClassField : fieldsInTestClass) {
                    logger.info(" scanning {} {} {} ", testClassField.getClass(), testClassField.getType(), testClassField.getName());

                    if (testClassField.getType().isAssignableFrom(WebDriver.class)) {
                        logger.info(" found what I was looking for: ");
                        Field driverField = result1.getTestClass().getRealClass().getDeclaredField(testClassField.getName());
                        driverField.setAccessible(true);
                        webDriver = (WebDriver) driverField.get(result1.getInstance());
                        logger.info("retrieved driver: {}", webDriver);
                        String browserSessionID = null;

                        if (webDriver.getClass().isAssignableFrom(ChromeDriver.class)) {
                            browserSessionID = ((ChromeDriver) webDriver).getSessionId().toString();
                        } else if (webDriver.getClass().isAssignableFrom(FirefoxDriver.class)) {
                            browserSessionID = ((FirefoxDriver) webDriver).getSessionId().toString();
                        }

                        Optional<BrowserMobProxy> browserMobProxy = Optional.ofNullable(proxyConcurrentMap.get(browserSessionID));
                        browserMobProxy.ifPresent(proxy -> {
                            currentListenerAssignedProxy.set(proxy);
                            logger.info("setting testID for this execution: {}", currentExecutionID);
                            logger.info(" is identified proxy running: {}", proxy.isStarted());
                            proxy.addHeader("X-TESTID", currentExecutionID);
                        });
                        break;
                    }
                }
            } catch (IllegalAccessException | SecurityException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        };
        listenerExecution.assignTestIDForCurrentExecution(result);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("***** Test Successfully Finished ***** {}", result.getName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        logger.info("***** Test Failed ***** {}", result.getName());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logger.info("***** Test Skipped ***** {}", result.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        logger.info("***** Test Failed but within success percentage ***** {}", result.getName());
    }

    @Override
    public void onStart(ITestContext context) {
        logger.info("***** This is onStart method ***** {}", context.getOutputDirectory());
    }

    @Override
    public void onFinish(ITestContext context) {
        logger.info("***** This is onFinish method ***** {}", context.getPassedTests());
        BrowserMobProxy browserMobProxy = currentListenerAssignedProxy.get();
        if (null != browserMobProxy && browserMobProxy.isStarted()) {
            logger.info("identified proxy instance to shutdown: {} @ {}", browserMobProxy, browserMobProxy.getPort());
            browserMobProxy.stop();
        }
    }
}
