package org.deployd.agent;

import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.SuperCall;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

//TODO implement per method proxy
// this is not scalable approach, since number of proxies running will be directly proportional to test cases
public class ProxyPerMethod {

    @RuntimeType
    public static Object intercept(@Origin Method method,
                                   @SuperCall Callable<?> callable) throws Exception {
        System.out.println("ProxyPerMethod Interceptor 0" );
        try {
            return callable.call();
        } finally {
            System.out.println("ProxyPerMethod Interceptor 1" );
        }
    }
}
