package org.deployd.agent;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.LoaderClassPath;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ClassFilePrinter;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.ClassMemberValue;
import javassist.bytecode.annotation.MemberValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Listeners;

import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;

/**
 * Javassist specific implementation to inject listeners.
 */
public class TestNgTransformer implements ClassFileTransformer {

    public static final Logger logger = LoggerFactory.getLogger(TestNgTransformer.class);
    String toWatchFor;

    @Override
    public byte[] transform(ClassLoader loader,
                            String className, Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) {
        byte[] byteCode = classfileBuffer;
        if (className.contains(toWatchFor) && !className.contains("$")) {
            logger.info(" -- className {}", className);
            ClassPool classPool = ClassPool.getDefault();
            classPool.appendClassPath(new LoaderClassPath(loader));
            CtClass ctClass;
            try {
                ctClass = classPool.get(className.replace("/", "."));
                if (ctClass.isFrozen()) {
                    ctClass.defrost();
                }
                ClassFile classFile = ctClass.getClassFile();
                ConstPool constPool = classFile.getConstPool();
                AnnotationsAttribute annotationsAttribute = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag);
                Annotation annotation = new Annotation(Listeners.class.getName(), constPool);
                MemberValue[] memberValues = new MemberValue[]{new ClassMemberValue(TestNgListener.class.getName(), constPool)};
                ArrayMemberValue arrayMemberValue = new ArrayMemberValue(constPool);
                arrayMemberValue.setValue(memberValues);
                annotation.addMemberValue("value", arrayMemberValue);
                annotationsAttribute.addAnnotation(annotation);
                ctClass.getClassFile().addAttribute(annotationsAttribute);
                if (logger.isDebugEnabled())
                    ClassFilePrinter.print(ctClass.getClassFile());
                byteCode = ctClass.toBytecode();
                ctClass.detach();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return byteCode;
    }
}
