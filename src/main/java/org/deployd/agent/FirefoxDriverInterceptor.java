package org.deployd.agent;

import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;

import java.lang.reflect.Constructor;

public class FirefoxDriverInterceptor {

    private FirefoxDriverInterceptor(){}

    @RuntimeType
    public static void intercept(@Origin Constructor<?> constructor) {
        System.out.println("Intercepted 1: " + constructor.getName());
    }
}
