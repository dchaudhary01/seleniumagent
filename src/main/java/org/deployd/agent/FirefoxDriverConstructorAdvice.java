package org.deployd.agent;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.implementation.bytecode.assign.Assigner;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.deployd.agent.TestAgent.proxyConcurrentMap;

public class FirefoxDriverConstructorAdvice {

    public static final Logger logger = LoggerFactory.getLogger(FirefoxDriverConstructorAdvice.class);
    private FirefoxDriverConstructorAdvice(){}
    public static ThreadLocal<BrowserMobProxy> constructedProxyPort = new ThreadLocal<>();

    @Advice.OnMethodEnter
    public static void before(
            @Advice.Origin String method,
            @Advice.AllArguments(readOnly = false, typing = Assigner.Typing.DYNAMIC) Object[] args) {
        logger.info("-----> instantiating firefoxwebdriver: {} {}", method, args);
        if (null == args || args.length == 0) {
            logger.info("apply default capabilities: {}", args);
            logger.info(" this will be handled with super call");
        } else {
            for (int k = 0; k < args.length; k++) {
                if (args[k] instanceof FirefoxOptions) {
                    logger.info(" -- {}", args[k]);
                    BrowserMobProxy proxy = new BrowserMobProxyServer();
                    FirefoxOptions firefoxOptions = ((FirefoxOptions) args[0]).merge(AgentUtil.getFirefoxProxyCapabilities(proxy));
                    constructedProxyPort.set(proxy);
                    args[k] = firefoxOptions;
                }
            }
        }
        logger.info("-----> pre args: {}", args);
    }

    @Advice.OnMethodExit
    public static void after(
            @Advice.Origin String method,
            @Advice.This Object thisObject
    ) {
        logger.info("<----- instantiated firefoxwebdriver: {} {}", method, thisObject);
        if (thisObject instanceof FirefoxDriver) {
            FirefoxDriver instantiatedObject = (FirefoxDriver) thisObject;
            logger.info("instantiatedObject: {} {}", instantiatedObject.getSessionId(), constructedProxyPort.get());
            proxyConcurrentMap.put(instantiatedObject.getSessionId().toString(), constructedProxyPort.get());
            logger.info(" capabilities on this instance {}", instantiatedObject.getCapabilities().asMap());
        } else {
            logger.error("cannot identify webdriver instance / proxy will instance will not be available");
        }
    }
}
